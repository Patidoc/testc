#! /bin/bash

NB_ERR=0
for F in *.c
do 
    echo "Compilation $F" 
    gcc $F -Wall -o $(echo $F | cut -d '.' -f1)
    NB_ERR=$(bc $NB_ERR + $?)
done
exit $NB_ERR